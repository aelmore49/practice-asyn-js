/* -------------- Callback approach for making HTTP Request Below: -------------- */

const getPuzzleCallBack = (wordCount, callBack) => {
	console.log(wordCount);
	// Making an HTTP request
	// Initialize request
	const request = new XMLHttpRequest();
	// Event Listener for when request is completed
	request.addEventListener("readystatechange", (e) => {
		let status = e.target.status;
		let readyState = e.target.readyState;
		if (readyState === 4 && status === 200) {
			const data = JSON.parse(e.target.responseText);
			callBack(undefined, data.puzzle);
		} else if (readyState === 4) {
			callBack("There was an error!", undefined);
		}
	});
	// Use the open method on our above request to initialize it.
	// It take two arguments: first is the the HTTP method and the second is the URL.
	request.open("GET", `http://puzzle.mead.io/puzzle?wordCount=${wordCount}`);
	// We use the send method to send out the request
	request.send();
};

const getCountryDetailsCallBack = (countryCode, callBack) => {
	const CC = countryCode.toUpperCase();
	const request = new XMLHttpRequest();
	request.addEventListener("readystatechange", (e) => {
		let status = e.target.status;
		let readyState = e.target.readyState;
		if (readyState === 4 && status === 200) {
			const data = JSON.parse(e.target.responseText);
			const countryCode = data.filter((code) => {
				if (code.alpha2Code === CC) {
					return code;
				}
			});

			callBack(undefined, countryCode[0]);
		} else if (readyState === 4) {
			callBack("There was an error!", undefined);
		}
	});
	request.open("GET", "https://restcountries.eu/rest/v2/all");

	request.send();
};

/* -------------- Using XMLHttpRequest() method for making Promise-Based HTTP Request: -------------- */

// Get Puzzle Promise
// Promises Shorthand syntax for an arrow function that returns a Promise

const getPuzzle = (wordCount) =>
	new Promise((resolve, reject) => {
		// Making an HTTP request
		// Initialize request
		const request = new XMLHttpRequest();
		// Event Listener for when request is completed
		request.addEventListener("readystatechange", (e) => {
			let status = e.target.status;
			let readyState = e.target.readyState;
			if (readyState === 4 && status === 200) {
				const data = JSON.parse(e.target.responseText);
				resolve(data.puzzle);
			} else if (readyState === 4) {
				reject("Puzzle Data Error!");
			}
		});
		// Use the open method on our above request to initialize it.
		// It take two arguments: first is the the HTTP method and the second is the URL.
		request.open("GET", `http://puzzle.mead.io/puzzle?wordCount=${wordCount}`);
		// We use the send method to send out the request
		request.send();
	});

// Get Country Promise
const getCountryDetails = (countryCode) =>
	new Promise((resolve, reject) => {
		const CC = countryCode.toUpperCase();
		const request = new XMLHttpRequest();
		request.addEventListener("readystatechange", (e) => {
			let status = e.target.status;
			let readyState = e.target.readyState;
			if (readyState === 4 && status === 200) {
				const data = JSON.parse(e.target.responseText);
				const countryCode = data.filter((code) => {
					if (code.alpha2Code === CC) {
						return code;
					}
				});

				resolve(countryCode[0]);
			} else if (readyState === 4) {
				reject("Error with Country Details Promise!");
			}
		});
		request.open("GET", "https://restcountries.eu/rest/v2/all");
		request.send();
	});

/* -------------- Using fetch() method for making Promise-Based HTTP Request Below: -------------- */

const getPuzzleFetch = (wordCount) => {
	return fetch(`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`, {})
		.then((response) => {
			if (response.status === 200) {
				return response.json();
			} else {
				throw new Error("Something went wrong!");
			}
		})
		.then((data) => {
			return data.puzzle;
		});
};

const getCountryDetailsFetch = (CC) => {
	return fetch(`https://restcountries.eu/rest/v2/all`, {})
		.then((response) => {
			if (response.status === 200) {
				return response.json();
			} else {
				throw new Error("Something went wrong!");
			}
		})
		.then((data) => {
			const countryCode = data.filter((code) => {
				if (code.alpha2Code === CC) {
					return code;
				}
			});

			return countryCode[0].name;
		});
};

const getLocation = () => {
	return fetch("http://ipinfo.io/json?token=9922d983c6dcbb", {})
		.then((response) => {
			if (response.status === 200) {
				return response.json();
			} else {
				throw new Error(" Oops! Something went wrong! =( ");
			}
		})
		.then((data) => data);
};

/* -------------------------- Fetch API: Using ASYNC-AWAIT  -------------------------- */

const getPuzzleFetchAsync = async (wordCount) => {
	const response = await fetch(
		`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`,
		{}
	);
	if (response.status === 200) {
		const data = await response.json();
		return data.puzzle;
	} else {
		throw new Error("Something went wrong!");
	}
};

const getCountryDetailsFetchAsync = async (CC) => {
	const response = await fetch(`https://restcountries.eu/rest/v2/all`, {});
	if (response.status === 200) {
		const data = await response.json();
		return data.find((country) => country.alpha2Code === CC).name;
	} else {
		throw new Error("Error returning country");
	}
};

const getCurrentCountry = async () => {
	const location = await getLocation();
	const country = await getCountryDetailsFetchAsync(location.country);
	return country;
};
