/* HTTP (Hypertext Transfer Protocol):
	HTTP is a request/response protocol, meaning, that we, the developer in the
	browser, issue some sort of request, which goes off to some third party server.
	That server does some work and it gives us back a response.

	Request: Describes what we the person making the request hope to do.
	Response: Contains information about what was actually done about said request.

	To view requests in a browser window, you have to go over to the network tab in the 
	developer tools. Once done, you can view a request's response by clicking on the "Response"
	tab. 

	Synchronous Code Execution: 
	Executing items in order, 
	from top to bottom, one item at a
	time. The previous item has to be completely done doing whatever it 
	is suppose to do before the program can move onto the next item. 

	Asynchronous Code Execution:
	Will skip items if they are
	not ready to execute, moving onto to execute items that are ready and
	then coming back to execute the previous items when they are ready. The 
	below function, getPuzzle, is an example of Asynchronous code. 

*/

const puzzleEl = document.querySelector("#puzzle");
const guessesEl = document.querySelector("#guesses");
let game1;

window.addEventListener("keypress", (e) => {
	const guess = String.fromCharCode(e.charCode);
	game1.makeGuess(guess);
	render();
});

const render = () => {
	puzzleEl.textContent = game1.puzzle;
	guessesEl.textContent = game1.statusMessage;
};

const startGame = async () => {
	const puzzle = await getPuzzle("2");
	game1 = new Hangman(puzzle, 5);
	console.log(game1);
	render();
};

document.querySelector("#reset").addEventListener("click", startGame);

startGame();
