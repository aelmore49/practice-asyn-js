/*
    * Handling errors inside of JS code:
    When we throw an error in JS, it crashes our program.
    We are allowed to provide a message and some context of where
    the problem is, letting the caller of the function know that they
    misused the function. 

    * To do this, we use the "throw" statement, which
    is a JS keyword, after throw we provide some more information about the
    error. 

    * When we throw errors, we want to be a descriptive as possible so that
    the person trying to recover from the error has enough information to do so.
    If we use a string when we throw an error, we just get that string back as the
    error message. 

    * There is a built-in JS function that provides a more comprehensive
    error message, which is the Error object. If you use the Error object when throwing an
    error, it will tell you where the error occurred in your program. 

    * Just because a program throws an error does not mean it needs to crash and burn.
    What is needed when throwing an error is the ability to recover from said error gracefully,
    meaning doing something other than letting things just explode. To do this, we use the
    "Try-Catch" statement. The Try-Catch statement starts out with a try block that has
    a set of curly braces, which will attempt to run a piece of code located within its curly
    braces. Directly after the try statement comes the catch block. The catch block takes in
    one argument, the error, and is executed if the piece of code in the preceding try block fails.
    The error argument passed into the catch block's parenthesis will return the error object and any
    throw statement used.
    Using Try-Catch statements allows one to recover from an error without letting the program completely crash.




*/

const getTip = (amount) => {
	if (typeof amount === "number") {
		return amount * 0.25;
	} else {
		throw Error("Must be a number!");
	}
};

try {
	const result = getTip(true);
	console.log(result);
} catch (e) {
	console.log("catch block is running", e);
}
