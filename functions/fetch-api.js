/* -------------------------- Fetch API:  --------------------------
	Fetch API:
	Is an alternative to using the XMLHttpRequest() method, 
	and has promises built into it.

	The fetch method takes two arguments, the URL for the request and
	a second optional argument where we can further customize our request.
	
	The fetch method returns a promise, so you can attach a then block to it and 
	wait for the response to be return. Furthermore, you can also attach a catch 
	block onto it to handle any possible errors. 

	Unlike the XMLHttpRequest() method, with the fetch API, we no longer have to 
	worry about the "readyState". The fetch method's promise is only going to resolve or
	reject when it is actually ready for us. With the fetch API, all we have to figure
	out is how the request completed.
	
	If an error happens or if things go wrong, we want our our fetch method's catch block to
	execute. If we want to trigger our fetch method's catch block on an error, we have to
	"throw" a new Error. Throwing a new Error will trigger the catch block to run inside of our
	fetch method. 

	Now we can figure out what to do with the data that the fetch method's
	promise returns when it is successful. On the response, we have access to a method
    called "json()". The json() method is going to take the response data and
    parse the data it into json. When we use the json() method on the response
    data, what gets returned is not a JS object, but is a promise, and the promise
    is going to resolve with the object at some point in the future.
	
	To get get the object data from the response's promise once it is resolved, we 
	can use promise chaining, by return the response.json() promise and then attaching a
	then block onto it so we can access the data. 

*/

// getLocation()
// 	.then((data) => {
// 		console.log(
// 			`You are currently in: ${data.city}, ${data.region}, ${data.country}`
// 		);
// 		return data.country;
// 	})
// 	.then((country) => {
// 		getCountryDetailsFetch(country)
// 			.then((data) => {
// 				console.log(data);
// 			})
// 			.catch((error) => {
// 				console.log(error);
// 			});
// 	})
// 	.catch((error) => {
// 		console.log(error);
// 	});

/* -------------------------- Fetch API: Using ASYNC-AWAIT  --------------------------*/
// getPuzzleFetchAsync()
// 	.then((data) => {
// 		console.log("PUZZLE ASYNC DATA:", data);
// 	})
// 	.catch((error) => {
// 		console.log("Error:", error);
// 	});

// getCountryDetailsFetchAsync("US")
// 	.then((data) => {
// 		console.log("COUNTRY ASYNC DATA:", data);
// 	})
// 	.catch((error) => {
// 		console.log(error);
// 	});

// getCurrentCountry()
// 	.then((data) => {
// 		console.log("Current Country Async Data:", data);
// 	})
// 	.catch((error) => {
// 		console.log(error);
// 	});
