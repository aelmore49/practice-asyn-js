// Prototypal Inheritance

class Person {
	constructor(firstName, lastName, age, likes = []) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.likes = likes;
		this.getBio;
		this.fullName;
	}
	getBio() {
		const [first, second, ...other] = this.likes;
		const bio = `${this.firstName} ${this.lastName} likes ${first} amongst other things and is ${this.age} years old.`;

		return bio;
	}

	set fullName(fullName) {
		const names = fullName.split(" ");
		this.firstName = names[0];
		this.lastName = names[1];
	}
	// The below getter is known as a "computed property", meaning,
	// the property in the getter does not actually exist.
	// We get the property based on other properties.
	get fullName() {
		return `${this.firstName} ${this.lastName}`;
	}
}

class Employee extends Person {
	constructor(firstName, lastName, age, likes = [], id, title, dept) {
		super(firstName, lastName, age, likes);
		this.id = id;
		this.title = title;
		this.dept = dept;
	}

	getBio() {
		const bio = `${this.fullName} works in the ${this.dept} department.`;
		return bio;
	}

	getYearsLeft() {
		let yearsLeft = 65 - this.age;
		return `${this.firstName} has ${yearsLeft} years left until he can retire.`;
	}
}

class Student extends Person {
	constructor(firstName, lastName, age, likes, grade) {
		super(firstName, lastName, age, likes);
		this.grade = grade;
	}

	getBio() {
		let currentGrade = this.grade;
		return currentGrade >= 70
			? `${this.firstName} is passing.`
			: `${this.firstName} is failing.`;
	}

	updateGrade(change) {
		this.grade += change;
	}
}

const studentOne = new Employee("John", "Doe", 18, ["Golf"], 420, "Dev", "IT");
console.log(studentOne.getBio());
