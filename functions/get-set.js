/* Getters and Setters in Js : allow us to customize what happens 
when someone sets an object property
    * With custom getters and setters, we can define logic to override the
   object property overriding.

*/

const data = {
	locations: [],
	get location() {
		return this._location;
	},
	set location(value) {
		this._location = value.toString().trim();
		this.locations.push(this._location);
	},
};

// code that uses the data object
data.location = " KCMO ";
data.location = " Seoul";

console.log(data.location);
console.log(data.locations);
