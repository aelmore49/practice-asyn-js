// Standard Callback
const getDataCallback = (num, callback) => {
	setTimeout(() => {
		if (typeof num === "number") {
			callback(undefined, num * 2);
		} else {
			callback("something went wrong!", undefined);
		}
	}, 2000);
};

// Below is an example of "Callback Hell"
getDataCallback(2, (error, data) => {
	if (error) {
		console.log(error);
	} else {
		getDataCallback(data, (error, data) => {
			if (error) {
				console.log(error);
			} else {
				console.log("Second time CB Data:", data);
			}
		});
	}
});

// Promise API

const getDataPromise = (num) =>
	new Promise((resolve, reject) => {
		setTimeout(() => {
			return typeof num === "number"
				? resolve(num * 2)
				: reject("Must be a number");
		}, 2000);
	});

// Below is an example of Promise Chaining
// When we return a promise from another promise's handler,
// we create promise chaining.

getDataPromise(2)
	.then((data) => {
		return getDataPromise(data);
	})
	.then((data) => {
		console.log("Chaining data:", data);
	})
	.catch((error) => {
		console.log(error);
	});
