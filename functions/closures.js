/*
	Closures in JS:
	Closures in JavaScript allow functions to close over their lexical scope,
	the environnement in which they were created in, giving them the ability to access and use variables
	defined outside out their function body.

	Currying in JS:
	A pattern which involves a process of transforming a single function that
	takes a lot of arguments into multiple functions that take a subset of those
	arguments.

	Curried Function Example:
	const createAdder = (a) => {
		console.log("a value:", a);
		return (b) => {
			console.log("b value:", b);
			return a + b;
		};
	};

	Non Curried Function Example:
	const add = (a,b)=> a+b;

*/

// Tipper function using currying pattern.

const createTipper = (baseTip) => {
	return (billAmount) => {
		const totalAmt = billAmount * baseTip;
		return `Total bill amount: ${totalAmt.toFixed(2)}`;
	};
};

const tip10Percent = createTipper(0.1);
console.log(tip10Percent(100.0));

const tip20Percent = createTipper(0.2);
console.log(tip20Percent(100));
