const gradeCalc = function (score, totalScore) {
	if (typeof score === "number" && typeof totalScore === "number") {
		const percent = (score / totalScore) * 100;
		let letterGrade = "";
		if (percent >= 90) {
			letterGrade = "A";
		} else if (percent >= 80) {
			letterGrade = "B";
		} else if (percent >= 70) {
			letterGrade = "C";
		} else if (percent >= 60) {
			letterGrade = "D";
		} else {
			letterGrade = "F";
		}
		return `You got a ${letterGrade} (${percent}%)!`;
	} else {
		throw Error("Must be a number!");
	}
};

try {
	const result = gradeCalc(19, 20);
	console.log(result);
} catch (e) {
	console.log(e.message);
}

/*
    Local Storage Variable:
    Is provided by the browser and gives us a way to store data.
    With Local Storage, you can preform the basic CRUD operations, 
    which are: Create, Read, Update and Delete. 

    Unlike Cookies, which are primarily used for reading server-side,
    Local Storage can only read client-side. 

*/

// 1) Create Data:
// To Create data via Local Storage, use the
// setItem method, which takes two strings as arguments, the key and value
localStorage.setItem("Thee_Way", "Code is life!");

// 2) Read Data:
// To Read data via Local Storage, use the
// getItem method, which takes one argument, the key.
// The value is what gets returned from the getItem method
let theWay = localStorage.getItem("Thee_Way");

// 3) Update Data:
// To Update data via Local Storage, like reading data,
// you use the setItem method with a different value
localStorage.setItem("Thee_Way", "Binary is Love!");

// 4) Delete Data:
// To Delete data  via Local Storage, use the
// removeItem method, which takes a single argument, the key for
// the value you want to delete.
localStorage.removeItem("Thee_Way");

// To Remove all data in Local Storage, you can use the clear method.

// Local Storage only supports strings.
// To store something other than a string value in Local Storage,
// you convert the data via JS to strings or to arrays.
// To do this, you use JavaScript Object Notation (JSON)

// JSON: is a lightweight data-interchange format. It is easy for humans to read and write.
// It is easy for machines to parse and generate.
// In JSON, keys and values are all in double quotes.

// The stringify method on JSON converts objects or other data into strings.
// The parse method on JSON converts JSON strings into JS objects.

const user = {
	name: "Alex",
	age: "36",
};

const userJson = JSON.stringify(user);

localStorage.setItem("userData", userJson);

const userItem = localStorage.getItem("userData");
const userParsed = JSON.parse(userItem);
console.log(userParsed);

const element = document.createElement("h1");
element.innerHTML = `${userParsed.name} is ${userParsed.age} years old.`;
document.body.append(element);
