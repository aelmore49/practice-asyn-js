/*
    Async-Await:
    Is a feature in JS. When we create a function, we can choose to  create
    a function as an "async" function, by adding the "async" keyword right before
    the function's definition.

    With the Async-Await feature, you can structure your code to look more synchronous, appearing
    to have one operation execute after another. 

    Async functions always return a promise, that promise is resolved with the value that
    you the developer decide to return from the function. 

    The Await Operator can only be used within an Async function. To use the Await operator, you 
    put the "await" keyword in front of a function declaration that returns a promise. 
    With the await operator, you do not have to use the then() to get the data from a promise. You
    can just store the await function in a const or variable.

    When using the Await Operator, you do not have to manually throw an error. The
    Await operator throws the error for you if the promise it is waiting on rejects.
    

*/

const getDataPromise = (num) =>
	new Promise((resolve, reject) => {
		setTimeout(() => {
			return typeof num === "number"
				? resolve(num * 2)
				: reject("Must be a number");
		}, 2000);
	});

const processData = async () => {
	let data = await getDataPromise("2");
	data = await getDataPromise(data);
	data = await getDataPromise(data);
	return data;
};

processData()
	.then((data) => {
		console.log("Data:", data);
	})
	.catch((error) => {
		console.log("Error:", error);
	});
